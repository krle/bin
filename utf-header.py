#!/usr/bin/env python
from email.header import decode_header
import sys

reload(sys)
sys.setdefaultencoding('utf8')

with open('/tmp/temp_subject.txt', 'w+') as inputFile:
    subject = raw_input()
    inputFile.write(subject)
    dh = decode_header(subject)
    default_charset = 'ASCII'
    result = u' '.join([ unicode(t[0], t[1] or default_charset) for t in dh ])
    inputFile.write(result)
    print(result)
